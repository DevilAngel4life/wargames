function betterThanAverage(classPoints, yourPoints) {
    var _total = 0;
    var _elements = 0;
    classPoints.forEach(function (element) {
        _total += element;
        _elements += 1;
    });
    var _ave = _total / _elements;
    if (yourPoints > _ave)
        return true;
    else
        return false;
}
var scores = [43, 54, 46, 76, 86, 86, 23];
var myScore = 60;
var average = betterThanAverage(scores, myScore);
console.log(average);
// this could be simplified alot more with properpties like reduce
// example is
//
//export function betterThanAverage(classPoints: number[], yourPoints: number) {
//     return classPoints.reduce((sum, num) => sum + num) / classPoints.length < yourPoints;
// }
