function gooseFilter(birds: string[]): string[] {
  const geese: string[] = [
    "African",
    "Roman Tufted",
    "Toulouse",
    "Pilgrim",
    "Steinbacher"
  ];

  // i need to do a for loop inside the array that was just passes
  // if it findsa matching return or continue without placing it into the
  // new array to be returned

  // return an array containing all of the strings in the input array except those that match strings in geese
  return birds;
}

let test: string[] = [
  "Mallard",
  "Hook Bill",
  "African",
  "Crested",
  "Pilgrim",
  "Toulouse",
  "Blue Swedish"
];
