/**
 *  Function that removes the white spaces from the string that is passed
 * and returns it.
 *
 * @param string x;
 * @returns string;
 */
function noSpace(x: string): string {
  return x.replace(/\s/g, "");
}
const inoSpace = (x: string): string => x.replace(/ /g, "");
let r = "8 j 8   mBliB8g  imjB8B8  jl  B";
let str = "hello world";
console.log(noSpace(r));
console.log(str.replace(/\s/, ""));
