function betterThanAverage(classPoints: number[], yourPoints: number) {
  let _total = 0;
  let _elements = 0;
  classPoints.forEach(element => {
    _total += element;
    _elements += 1;
  });
  let _ave = _total / _elements;
  if (yourPoints > _ave) return true;
  else return false;
}

let scores = [43, 54, 46, 76, 86, 86, 23];
let myScore = 60;

let average = betterThanAverage(scores, myScore);
console.log(average);

// this could be simplified alot more with properpties like reduce
// example is
//
//export function betterThanAverage(classPoints: number[], yourPoints: number) {
//     return classPoints.reduce((sum, num) => sum + num) / classPoints.length < yourPoints;
// }
